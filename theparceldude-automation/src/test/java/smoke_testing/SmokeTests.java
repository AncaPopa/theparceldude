package smoke_testing;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page_object.*;
import utils.BaseTestClass;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SmokeTests extends BaseTestClass {

    @Test
    public void registerNewAccount() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        ConnectPage connectPage = homeShopPage.clickOnConnect();
        connectPage.getPageTitle();
        assertEquals("CONNECT", connectPage.getPageTitle());
        connectPage.fillInRegisterFields("firstName@gmail.com" + System.currentTimeMillis(),
                "A.123456!", "popescu", "florescu", "0232");
        connectPage.clickOnRegisterButton();
        assertEquals("https://testare-automata.practica.tech/shop/", driver.getCurrentUrl());
    }

    @Test
    public void checkSearchResults() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnSearchIcon();
        SearchResultPage searchResults = homeShopPage.typeSearch("dress");
        assertTrue(searchResults.checkResultsContainsSearch("dress"));
    }

    @Test
    public void loginOnPage() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        ConnectPage connectPage = homeShopPage.clickOnConnect();
        connectPage.fillInLoginFields("firstName@gmail.com1610019190650", "A.123456!");
        connectPage.clickOnRememberMeButton();
        List<WebElement> loginMessage = driver.findElements(By.xpath("//div[@class='woocommerce-MyAccount-content']/p[1]"));
        assertTrue(loginMessage.isEmpty());
        connectPage.clickOnLogin();
        connectPage.getLoginMessage();
        assertEquals("Hello firstname-5566 (not firstname-5566? Log out)", connectPage.getLoginMessage());
    }

    @Test
    public void addToCartFromImage() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnCategory();
        CategoryPage categoryPage = homeShopPage.clickOnMenCollection();
        categoryPage.addToCartFromHoveredIcon();

        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.textToBePresentInElement(By.className("cart-total"), "70,00 lei"));

        assertEquals(categoryPage.getProductPrice(), categoryPage.getTotalCartValue());
        assertEquals(1, categoryPage.getCartValue());
    }

    @Test
    public void addToCartAndCheckCartValue() {
        SingleProductPageFactory singleProductPageFactoryObj = new SingleProductPageFactory(driver);
        SingleProductPageFactory singleProductPageFactory = singleProductPageFactoryObj.addToCart(driver);
        assertTrue(singleProductPageFactory.getCartMessage().isDisplayed());
        assertEquals(1, singleProductPageFactory.getCartValue());
        assertTrue(singleProductPageFactory.getProductPrice().equals(singleProductPageFactory.getCartProductPrice()));
    }

    @Test
    public void checkout() {
        SingleProductPageFactory singleProductPageFactoryObj = new SingleProductPageFactory(driver);
        SingleProductPageFactory singleProductPageFactory = singleProductPageFactoryObj.addToCart(driver);
        CartPage cartPage = singleProductPageFactory.clickOnViewCartButton();
        assertEquals("https://testare-automata.practica.tech/shop/?page_id=9", driver.getCurrentUrl());
        CheckoutPage checkoutPage = cartPage.clickOnProceedToCheckoutButton();
        checkoutPage.fillInBillingDetails("Cluj", "test", "test1",
                "11 Sucevei", "Cluj", "123456", "0232123456", "test@gmail.com",
                "StrongPassword.1!");
        checkoutPage.ShipToDifferentAddressCheckBox();
        checkoutPage.clickOnPlaceOrderButton();
        assertEquals("Thank you. Your order has been received.", checkoutPage.getOrderMessage());
        assertEquals("20,00 lei", checkoutPage.cartSubtotal());
    }
}

