package page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage {
    WebDriver driver;

    public CartPage(WebDriver driver) {
        this.driver = driver;
    }

    private final By CART_TITLE = By.className("entry-title");
    private final By CART_MESSAGE = By.className("cart-empty");
    private final By CART_VALUE = By.className("cart-value");
    private final By CHECKOUT_BUTTON = By.className("checkout-button");


    public String getCartTitle() {
        return driver.findElement(CART_TITLE).getText();
    }

    public String getCartMessage() {
        return driver.findElement(CART_MESSAGE).getText();
    }

    public int getCartValue() {
        return Integer.parseInt(driver.findElement(CART_VALUE).getText());
    }

    public CheckoutPage clickOnProceedToCheckoutButton() {
        driver.findElement(CHECKOUT_BUTTON).click();
        return new CheckoutPage(driver);
    }

}
